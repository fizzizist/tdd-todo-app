import os
import re
import imaplib
import time

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.core import mail

SUBJECT = 'Your login link for Superlists'


class FunctionalTest(StaticLiveServerTestCase):

    def set_browser(self):
        """Sets self.browser by getting the remote webdriver."""
        options = webdriver.FirefoxOptions()
        options.add_argument('--ignore-ssl-errors=yes')
        options.add_argument('--ignore_certificate-errors')

        selenium_url = 'http://selenium:4444/wd/hub'

        self.staging_server_url = os.environ.get('STAGING_SERVER_URL')
        if self.staging_server_url:
            self.live_server_url = self.staging_server_url
            selenium_url = os.environ.get('SELENIUM_URL')

        self.browser = webdriver.Remote(command_executor=selenium_url, options=options)

    def setUp(self):
        self.set_browser()

    def tearDown(self):
        self.browser.quit()

    @classmethod
    def setUpClass(cls):
        if not os.environ.get('STAGING_SERVER_URL', False):
            cls.host = 'web'
        super(FunctionalTest, cls).setUpClass()

    def wait_for_row_in_list_table(self, row_text):
        selector = f'//table[@id="id_list_table"]//td[contains(text(), "{row_text}")]'
        row = WebDriverWait(self.browser, 10).until(EC.presence_of_element_located((By.XPATH, selector)))
        self.assertIn(row_text, row.text)

    def get_item_input_box(self):
        return self.browser.find_element(by=By.ID, value='id_text')

    def log_in(self):
        if self.staging_server_url:
            test_email = os.environ['EMAIL_USER']
        else:
            test_email = 'fake@example.com'

        self.browser.get(self.live_server_url)
        self.browser.find_element(by=By.NAME, value='email').send_keys(test_email)
        self.browser.find_element(by=By.NAME, value='email').send_keys(Keys.ENTER)

        WebDriverWait(self.browser, 10).until(
            EC.presence_of_element_located((By.XPATH, "//div[text()[contains(.,'Check your email')]]"))
        )

        body = self.wait_for_email(test_email, SUBJECT)

        self.assertIn('Use this link to log in', body)
        url_search = re.search(r'http://.+/.+$', body)
        if not url_search:
            self.fail(f'Could not find url in email body:\n{body}')
        url = url_search.group(0)
        self.assertIn(self.live_server_url, url)

        self.browser.get(url)

        self.wait_to_be_logged_in(test_email)

        return test_email

    def wait_to_be_logged_in(self, email):
        WebDriverWait(self.browser, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, 'Log out'))
        )
        navbar = self.browser.find_element(by=By.CSS_SELECTOR, value='.navbar')
        self.assertIn(email, navbar.text)

    def wait_to_be_logged_out(self, email):
        WebDriverWait(self.browser, 10).until(
            EC.presence_of_element_located((By.NAME, 'email'))
        )
        navbar = self.browser.find_element(by=By.CSS_SELECTOR, value='.navbar')
        self.assertNotIn(email, navbar.text)

    def wait_for_email(self, test_email, subject):
        if not self.staging_server_url:
            email = mail.outbox[0]
            self.assertIn(test_email, email.to)
            self.assertEqual(email.subject, subject)
            return email.body

        email_id = None
        start = time.time()
        inbox = imaplib.IMAP4_SSL('imap.gmail.com')
        try:
            inbox.login(test_email, os.environ['EMAIL_PASSWORD'])
            inbox.select('inbox')
            while time.time() - start < 60:
                _, data = inbox.search(None, 'ALL')
                for num in data[0].split():
                    _, msg = inbox.fetch(num, '(RFC822)')
                    msg = msg[0][1].decode('utf8')
                    if f'Subject: {subject}' in msg:
                        email_id = num
                        return msg
                time.sleep(5)
        finally:
            if email_id:
                inbox.store(email_id, "+FLAGS", "\\Deleted")
            inbox.close()
            inbox.logout()
