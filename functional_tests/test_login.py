from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from .base import FunctionalTest


class LoginTest(FunctionalTest):

    def test_can_get_email_link_to_log_in(self):
        test_email = self.log_in()

        WebDriverWait(self.browser, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, 'Log out'))
        ).click()

        self.wait_to_be_logged_out(test_email)
